FROM openjdk

MAINTAINER jyang

RUN mvn clean package -DskipTests=true -U \
    && mkdir -p /usr/local/testdocker

COPY target/demo-0.0.1-SNAPSHOT.jar /usr/local/testdocker/testdocker.jar

ENTRYPOINT ["java","-jar","/usr/local/testdocker/testdocker.jar"]




