package com.example.demo.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.demo.common.PageData;
import java.util.List;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
public class PageDataConvert {

    public static <T> PageData<T> pageConvert(IPage<T> page) {
        PageData<T> pageData = new PageData<>((int)page.getCurrent(), (int)page.getSize());
        pageData.setRows(page.getRecords());
        pageData.setTotalCount(page.getTotal());
        pageData.setTotalPage((int) page.getPages());
        return pageData;
    }

    public static <T> PageData<T> pageConvert(IPage<?> page, List<T> data) {
        PageData<T> pageData = new PageData<>((int)page.getCurrent(), (int)page.getSize());
        pageData.setRows(data);
        pageData.setTotalCount(page.getTotal());
        pageData.setTotalPage((int) page.getPages());
        return pageData;
    }

}
