package com.example.demo.controller;

import com.example.demo.common.PageData;
import com.example.demo.common.Result;
import com.example.demo.model.req.BlocSearchReq;
import com.example.demo.model.vo.BlocPageVO;
import com.example.demo.service.BlocService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/30
 **/
@Api(tags = "后台集团管理")
@RestController
@RequestMapping("/bloc")
public class BackendBlocController {

    @Autowired
    private BlocService blocService;

    @ApiOperation("分页查询集团列表")
    @PostMapping("/queryPage")
    public Result<PageData<BlocPageVO>> queryPage(@RequestBody BlocSearchReq req) {
        return Result.ok(blocService.queryPage(req));
    }
}
