package com.example.demo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 开关状态。ONLINE，OFFLINE
 *
 * @author huangyong
 */
@Getter
@AllArgsConstructor
public enum SwitchStatus {

    ONLINE("启用") ,
    OFFLINE("禁用") ;

    /**
     * 描述
     */
    private final String describe;

    @Override
    public String toString() {
        return describe;
    }


}
