package com.example.demo.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.common.PageData;
import com.example.demo.mapper.BaseBlocMapper;
import com.example.demo.model.po.BaseBlocPO;
import com.example.demo.model.req.BlocSearchReq;
import com.example.demo.model.vo.BlocPageVO;
import com.example.demo.service.BlocService;
import com.example.demo.util.PageDataConvert;
import java.util.Objects;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
@Service
public class BlocServiceImpl extends ServiceImpl<BaseBlocMapper, BaseBlocPO> implements BlocService {

    @Override
    public PageData<BlocPageVO> queryPage(BlocSearchReq req) {
        IPage<BlocPageVO> blocPage = lambdaQuery().eq(Objects.nonNull(req.getId()), BaseBlocPO::getId, req.getId())
                .eq(ObjectUtil.isNotEmpty(req.getBlocCode()), BaseBlocPO::getBlocCode, req.getBlocCode())
                .like(ObjectUtil.isNotEmpty(req.getBlocName()), BaseBlocPO::getBlocName, req.getBlocName())
                .eq(Objects.nonNull(req.getStatus()), BaseBlocPO::getStatus, req.getStatus())
                .orderByDesc(BaseBlocPO::getUpdateTime)
                .page(Page.of(req.getPageNum(), req.getPageSize()))
                .convert(bloc -> {
                    BlocPageVO pageVO = new BlocPageVO();
                    BeanUtils.copyProperties(bloc, pageVO);
                    return pageVO;
                });
        return PageDataConvert.pageConvert(blocPage);
    }
}
