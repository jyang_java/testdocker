package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.common.PageData;
import com.example.demo.model.po.BaseBlocPO;
import com.example.demo.model.req.BlocSearchReq;
import com.example.demo.model.vo.BlocPageVO;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
public interface BlocService extends IService<BaseBlocPO> {

    PageData<BlocPageVO> queryPage(BlocSearchReq req);
}
