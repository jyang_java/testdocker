package com.example.demo.common;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
@Data
public class PageQuery implements Serializable {

    @ApiModelProperty(value = "页码")
    private Integer pageNum = 1;

    @ApiModelProperty(value = "页数")
    private Integer pageSize = 10;
}
