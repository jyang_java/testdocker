package com.example.demo.common;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
@Data
public class PageData<T> implements Serializable {

    @ApiModelProperty(value = "总行数,总条数")
    private long totalCount;

    @ApiModelProperty(value = "当前页码")
    private int pageNum = 1;

    @ApiModelProperty(value = "数量")
    private int pageSize = 10;

    @ApiModelProperty(value = "总页数")
    private int totalPage;

    @ApiModelProperty(value = "结果集合")
    private List<T> rows;

    public PageData(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
}
