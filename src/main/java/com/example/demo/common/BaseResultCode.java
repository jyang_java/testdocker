package com.example.demo.common;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
public enum BaseResultCode implements IBaseResultCode  {

    SUCCESS("1", "success"),
    ;

    /**
     * 返回码
     */
    private final String code;

    /**
     * 返回码描述
     */
    private final String desc;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    BaseResultCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
