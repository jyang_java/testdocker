package com.example.demo.common;

import java.io.Serializable;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
public interface IBaseResultCode extends Serializable {

    /**
     * 返回code
     * @return code
     */
    String getCode();

    /**
     * 返回描述
     * @return 描述
     */
    String getDesc();
}
