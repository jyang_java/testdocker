package com.example.demo.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import lombok.Data;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/30
 **/
@Data
public class Result<T> implements Serializable {

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回码描述
     */
    private String desc;

    /**
     * 返回对象
     */
    private T data;

    /**
     * 异常处理堆栈
     */
    @JsonIgnore
    protected transient Throwable exception;

    public static <T> Result<T> ok() {
        return new Result<>(BaseResultCode.SUCCESS);
    }

    public static <T> Result<T> ok(T t) {
        return new Result<>(t);
    }

    public Result() {
        this(BaseResultCode.SUCCESS);
    }

    protected Result(T data) {
        this();
        this.data = data;
    }

    protected Result(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    protected Result(String code, String desc, T data) {
        this.code = code;
        this.desc = desc;
        this.data = data;
    }

    protected Result(IBaseResultCode iBaseResultCode) {
        this.code = iBaseResultCode.getCode();
        this.desc = iBaseResultCode.getDesc();
    }

    protected Result(IBaseResultCode iBaseResultCode, Throwable exception) {
        this.code = iBaseResultCode.getCode();
        this.desc = iBaseResultCode.getDesc();
        this.exception = exception;
    }


    protected Result(String code, String desc, Throwable exception) {
        this.code = code;
        this.desc = desc;
        this.exception = exception;
    }
}
