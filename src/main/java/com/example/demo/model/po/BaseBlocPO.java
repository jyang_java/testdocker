package com.example.demo.model.po;

import static com.baomidou.mybatisplus.annotation.FieldStrategy.NEVER;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.example.demo.enums.SwitchStatus;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/30
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_base_bloc")
public class BaseBlocPO extends Model<BaseBlocPO> {

    /**
     * 主键自增
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 集团代码
     */
    @TableField("bloc_code")
    private String blocCode;

    /**
     * 集团名称
     */
    @TableField("bloc_name")
    private String blocName;

    /**
     * 状态
     */
    @TableField("status")
    private SwitchStatus status;

    /**
     * 创建时间
     * 数据库中自动赋值
     */
    @TableField(value = "create_time", insertStrategy = NEVER, updateStrategy = NEVER)
    private LocalDateTime createTime;

    /**
     * 创建操作人
     */
    @TableField(value = "create_author", fill = FieldFill.INSERT)
    private String createAuthor;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", insertStrategy = NEVER, updateStrategy = NEVER)
    private LocalDateTime updateTime;

    /**
     * 修改操作人
     */
    @TableField(value = "update_author", fill = FieldFill.UPDATE)
    private String updateAuthor;
}
