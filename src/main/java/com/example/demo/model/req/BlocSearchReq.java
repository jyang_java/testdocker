package com.example.demo.model.req;

import com.example.demo.common.PageQuery;
import com.example.demo.enums.SwitchStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class BlocSearchReq extends PageQuery {

    @ApiModelProperty("集团id")
    private Long id;

    @ApiModelProperty("集团代码")
    private String blocCode;

    @ApiModelProperty("集团名称")
    private String blocName;

    @ApiModelProperty("上下架状态")
    private SwitchStatus status;
}
