package com.example.demo.model.vo;

import com.example.demo.enums.SwitchStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 集团列表返回累
 * @Author: zhouxueqing
 * @Date: 2022/7/31
 **/
@Data
public class BlocPageVO {

    @ApiModelProperty("集团id")
    private Long id;

    @ApiModelProperty("集团代码")
    private String blocCode;

    @ApiModelProperty("集团名称")
    private String blocName;

    @ApiModelProperty("上下架状态")
    private SwitchStatus status;

    @ApiModelProperty("最后更新人")
    private String updateAuthor;

    @ApiModelProperty("最后更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
