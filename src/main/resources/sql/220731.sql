CREATE TABLE `t_base_bloc` (
                               `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                               `bloc_code` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '集团代码',
                               `bloc_name` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '集团名称',
                               `status` enum('ONLINE','OFFLINE') COLLATE utf8mb4_bin NOT NULL DEFAULT 'OFFLINE' COMMENT '状态',
                               `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               `create_author` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建操作人',
                               `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                               `update_author` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改操作人',
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `unique_bloc_code` (`bloc_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='基础配置-集团表';
